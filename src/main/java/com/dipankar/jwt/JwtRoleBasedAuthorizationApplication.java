package com.dipankar.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtRoleBasedAuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtRoleBasedAuthorizationApplication.class, args);
	}

}
