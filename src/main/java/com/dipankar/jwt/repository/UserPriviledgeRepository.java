package com.dipankar.jwt.repository;

import com.dipankar.jwt.entity.UserPriviledge;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface UserPriviledgeRepository extends CrudRepository<UserPriviledge, Integer> {
    UserPriviledge findByUserName(String userName);

}
