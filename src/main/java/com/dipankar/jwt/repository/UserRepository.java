package com.dipankar.jwt.repository;

import com.dipankar.jwt.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User,String> {
     User findByUsername(String userName);
}
