package com.dipankar.jwt.controller;

import com.dipankar.jwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/myapp")
public class DemoController {
    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("/home")
    public String home(){
        return "This should be viewed by any logged in user";
    }

    @GetMapping("/admin")
    public String adminHome(@RequestHeader("Authorization") String bearerToken){
       if("Admin".equalsIgnoreCase(jwtUtil.getClaimPropertyValue(bearerToken,"role")))
           return "This should be only viewed by Admin role :" ;
       else
           return "You are not authorized to view this page";
    }

    @GetMapping("/client1/product")
    public String client1Product(@RequestHeader("Authorization") String bearerToken){
        if("Client1".equalsIgnoreCase(jwtUtil.getClaimPropertyValue(bearerToken,"client")))
            return "This can be viewed by logged in user with client 1 :" ;
        else
            return "You are not authorized to view Client 1 information";
    }

    @GetMapping("/client2/product")
    public String client2Product(@RequestHeader("Authorization") String bearerToken){
        if("Client2".equalsIgnoreCase(jwtUtil.getClaimPropertyValue(bearerToken,"client")))
            return "This can be viewed by logged in user with client 2 :" ;
        else
            return "You are not authorized to view Client 2 information";
    }

}
