package com.dipankar.jwt.controller;

import com.dipankar.jwt.dto.AuthUserDto;
import com.dipankar.jwt.dto.ErrorResponse;
import com.dipankar.jwt.dto.LoginRequest;
import com.dipankar.jwt.dto.LoginResponse;
import com.dipankar.jwt.entity.*;
import com.dipankar.jwt.repository.UserPriviledgeRepository;
import com.dipankar.jwt.repository.UserRepository;
import com.dipankar.jwt.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/myapp/auth")
public class LoginController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserPriviledgeRepository userPriviledgeRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtil jwtUtil;

    @ResponseBody
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResponseEntity login(@RequestBody LoginRequest loginReq)  {

        try {

            /*
               This in turn calls the loadUserByUserName(String username) method. Because in authentication manager we have provided
               the implementation service name. However, this is not mandatory to invoke. Instead,we can call any other custom method to authenticate.
             */
           authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginReq.getUserName(), loginReq.getPassword()));

            // Get User Role and other information required for creating JwtToken payload and pass it to createJwToken()
            UserPriviledge userPriviledge = userPriviledgeRepository.findByUserName(loginReq.getUserName());
            AuthUserDto authUserDto = new AuthUserDto();
            authUserDto.setUsername(loginReq.getUserName());
            authUserDto.setRole(userPriviledge.getRole());
            authUserDto.setClient(userPriviledge.getClient());
            authUserDto.setTenantid(userPriviledge.getTenantId());

            String token = jwtUtil.createToken(authUserDto);

            LoginResponse loginResponse = new LoginResponse(loginReq.getUserName(),token);
            return ResponseEntity.ok(loginResponse);

        }catch (BadCredentialsException e){
            ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST,"Invalid username or password");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }catch (Exception e){
            ErrorResponse errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }
}
