package com.dipankar.jwt.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class User {

    @Id
    @Column ( name = "USER_NAME")
    private String username;
    private String password;

}
