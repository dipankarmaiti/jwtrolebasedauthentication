package com.dipankar.jwt.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "USER_PRIVILDEG")
@Data
public class UserPriviledge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column ( name = "USER_NAME")
    private String userName;
    private String role;
    private String client;
    private String tenantId;

}
